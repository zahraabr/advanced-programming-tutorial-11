# The Will of a King

Almost two months have passed after []’s death . You and A.I are still in the small city of the empire, hiding from Aku Ganteng.  In that two months you have been busy searching for information about The Seven Wonders. You also have created an infrastructure of the new system you want to use to control the entire Empire from this emergency capital city. 

The citizens of the empire recognize you as [] (empty list), the ruler of this very empire and also one of the strongest beings in the world known as Wonders. But you know the bitter fact that you are not the king and you do not possess the power of a wonder.  From the information you got from some sources, your chances to win a fight against Aku Ganteng with raw power is almost 0. You know only a wonder can defeat another wonder. Unfortunately you are not a wonder and you don’t know where the other wonders are. At least for now you can be sure you will find a wonder soon, as you smile preparing the system you want to deploy. 

Until now, you have successfully gained the trust of the citizens of the empire. They acknowledge you as their leader. They respect you so much. They think you are the saviour of this empire. Well all of this is possible because you manipulate their memory, thanks to A.I’s power. You are not doing anything against your will yet. Not yet. You know you will do it soon or later. Even if you try to deny it, even if you know this is not the right way to save this empire, you can’t do anything but accept the fact that what has to be done, must be done. 

You sit at your workspace in the small mansion. A.I stands near to you as always to assist your work. A.I is splendid in her own way you think. She can use various powers such as magic, Mage Aspect, even some Lost Attributes; She can create programs and know some best practices. You can say you can’t ask for a better servant. 

“ So what are you doing now? You are done with the main features, haven’t you? Why haven't you deployed it yet? “ 

A.I glares at your screen as she is curious about what you intend to do. She knows you have already finished the feature. She also knows that the empire doesn’t have the luxury of slacking around because of Aku Ganteng’s threat. Every action has to be done quickly and precisely. 

“ I know. But I just want to make sure that the application has met all the minimum criteria to be deployed.  If they don’t, I’m afraid to do so. It will pose more danger than benefit to us. Of course I can't let that happen. “

“ Like security and reliability? “

“ Precisely. I want it to follow these requirements. “ you reply to her as you give her a piece of paper containing the requirements you mentioned earlier.

“ 12 Factor? I never heard this before. “ 

“ Well that is the very reason [] summon me to this world. “

“ Yeah, you are right. “

A.I reads and starts to analyze the document very carefully. Sometimes, she uses her magic to write down the important part of the contents. 

“ So there are 12 Factors** that represent the 12 requirements you mentioned earlier. Is that correct?“

“ Yes. But you should know by now that not all the factors are feasible with the time we have right now. So I decided to use just some of them and cross out some that are not necessary. “

“ Oh do you mean you will use all the factors that are circled? “

“ Yes. About that, can you help me to list all the requirements that have been completed and the one that are not yet to be completed?“

“ As you wish. “

**: you refers to this [link](https://12factor.net/)

Some minutes have passed since A.I started analyzing the applications. In that time you begin to think about the distant future. Actually that word is not correct. You start to think about the near future. Once you have deployed the application, you will have to use it against your will. You will create the magic knight using the citizens that are available. You know the consequences of making a magic knight. They will forget who they are. It is just the same as taking everything from them. You are becoming the very thing you swore to destroy. But what has to be done, must be done. You know you are willing to be a villain just to achieve your goal of saving the empire. In the end evil or good, dark or white are just words. The meaning can vary with context. 

“ It is done. “ 

“ Thanks. “ you take the result of analysis from A.I’s hand and read it carefully.

```
1. Codebase: X no staging and production development, only development
2. Dependencies: V - Using Gradle automatic build system
3. Config: X   Some of variable are hard coded
4. Backing Service: V - Using dependencies system)
5. Build, Release, run: X
6. (Not included)
7. Port Binding: X and V. Need to be declared specifically in applications.properties
8. (Not included)
9. (Not included)
10. Dev/prod parity: X need to ensure point 3, since hard coding make it coupled to local development environment
11. Logs: X Some classes don't have loggers and some of the debugging’s print are not yet using logger. 
12. Admin processes - V has been implemented by spring boot by default. 

```  
After you finish reading, you start to make the report more detailed. The results are presented below.


## Codebase

Any application should have exactly one codebase. What is a codebase? You know it as a Git repository you already know. Although technically the application used for this job already satisfies this constraint, the application still has one environment, that is development environment. The codebase constraint restricts the application without regarding the number of servers that run the application. This means every server that runs the application will use the same codebase.

*Checklist*
- [ ] Create a branch named “staging”.
- [x] Create a Heroku account if you still don’t have one.
- [x] Create a Heroku app named “advprog-tutorial11-<NPM>-production” where <NPM> is your student identification number (also eponymously known as NPM). This will be your production environment.
- [x] Create another Heroku app named “advprog-tutorial11-<NPM>-staging” where <NPM> is the same value from before. This will be your staging environment.
- [x] Create Heroku Postgres instances for both servers.
- [ ] Deploy the application to both servers. You must deploy the code from the “master” branch to the production environment and deploy the code from the “staging” branch to the staging environment. (Hint: You might want to solve the “Build, release, run” constraint’s task to do this)

## Config

The application configuration in application.properties need to not include some sensitive information like database username, password. Their value has to be taken from environment variables.

*Checklist*
 
- [x]  Analyze all variables in the application.properties. List all dangerous variables that store sensitive information. 
- [x]  Make environments variables to store sensitive information
- [x]  Refactor application.properties to use the environment variables.

## Build Release and run

Application deployment should be separated into three stages: build, release, run. 

Build stage is when “transformation” of the application’s code into executable files is done. Release stage is when the application’s code that already passed the build stage is sent to the deployment’s environment (for example: staging and deployment). Run stage is when the application’s code that passed the release stage is run (in the designed environment in release stage). Your job is to make the deployment flow into these stages.

*Checklist*
- [x] Create “.gitlab-ci.yml”. You can use the option “Set up CI/CD” in your repository’s page. (Notes : Make sure you use the already defined Gradle template. Change the image into “gradle:jdk11”)
- [x] Create build and release stages into your “.gitlab-ci.yml”. Make sure to differentiate release stages for staging and production environments
- [x] Make sure not to expose sensitive information related to your Heroku apps and account. (Hint: Satisfy the Config constraint)

## Port Binding

As a web application it is common sense that it will run on port 80. But this time you want the service to serve at 8089. It also makes the application have an universal port.

*Checklist*
- [x] Change the port at application.properties

## Dev/prod parity

At the current state, the application will run smoothly in a local development environment. It will not run at gitlab or heroku. Provide the each environment (refers to staging and production at point 1) the variable needed to run the application. At the current state, the application has been using the same database driver as the production. You just need to make sure the data sources are correctly configured. In the deal condition, the OS that runs the application should be the same in all environments. But you will assess this issue later. 

*Checklist*
- [x] Provide all the variables needed in the repository as well in the heroku.

## Logs


The last thing you need to do is to implement a logging system. During production, we don’t always want all the system’s information to be displayed. Say we print out every little detail using `System.out.println()`. Sometimes we don’t want this information to leak, or sometimes it returns a simple confirm message which isn’t really necessary. This is bad granularity. Using a logger, we can control the levels of logging output we want.

There are several ways to implement a logger, either with java.util or an external application. You are not confined to only one method to log messages to the console. The main point is to treat your logs as event streams, and output to `stdout`. What the environment chooses to do with these event streams are of the environment’s concern. All the application needs to do is output logs based on the events that are happening within the application.

*Checklist*

- [x] Implement a logging system for the functions. Make sure it can return an output to stdout. You are allowed to use any logging system you want. (Hint: lombok has a logging annotation)

Side Note:
```
You also need to create a new gitlab repository for this tutorial. As mentioned above, you also need to create some heroku applications. Please reply to the tutorial forum in the Scele to provide the information of your new created repository and heroku applications. 

Please use the format below:

[Student ID] - [Name] - [gitlab repository for this tutorial] - [heroku staging] -[heroku production]

Make sure your repository’s visibility is private. Please invite all teaching assistant’s account to your new created repository.
```
