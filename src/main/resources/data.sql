INSERT INTO public.role (id, role) VALUES (1, 'ADMIN') ON CONFLICT DO NOTHING;
INSERT INTO public.role (id, role) VALUES (2, 'USER') ON CONFLICT DO NOTHING;

INSERT INTO public.users (username, password, id_role) VALUES ('mahaadmin', '$2a$10$eyfZJ2AXLPXdzqFg8NVtZ.N5zrWqhZYGPHSU7kTREtnW7IK92mmAK', 1) ON CONFLICT DO NOTHING;

INSERT INTO public.magics (id, name, type) VALUES (4, 'Alacrity', 'SUPPORT') ON CONFLICT DO NOTHING;
INSERT INTO public.magics (id, name, type) VALUES (5, 'Sun Strike', 'ATTACK') ON CONFLICT DO NOTHING;
INSERT INTO public.magics (id, name, type) VALUES (6, 'Chaos Meteor', 'ATTACK') ON CONFLICT DO NOTHING;
INSERT INTO public.magics (id, name, type) VALUES (7, 'Ice Wall', 'DEFENSE') ON CONFLICT DO NOTHING;

INSERT INTO public.magic_knights (name, position, magic_id) VALUES ('Kanon', 'NORTH', 7) ON CONFLICT DO NOTHING;
INSERT INTO public.magic_knights (name, position, magic_id) VALUES ('Hassan', 'EAST', 6) ON CONFLICT DO NOTHING;