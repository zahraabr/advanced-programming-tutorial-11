package id.ac.ui.cs.tutorial11.enumattr;

public enum MagicType {
    ATTACK, DEFENSE, SUPPORT;

    public static MagicType getInstance(String type){
        if(type.equals("attack")){
            return MagicType.ATTACK;
        } else if(type.equals("defense")){
            return MagicType.DEFENSE;
        } else {
            return MagicType.SUPPORT;
        }
    }
}