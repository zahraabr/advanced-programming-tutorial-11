package id.ac.ui.cs.tutorial11.repository;

import id.ac.ui.cs.tutorial11.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<RoleModel, Long> {
    RoleModel findById(@Param("id")long id);
    List<RoleModel> findByRole(@Param("role")String role);

}
