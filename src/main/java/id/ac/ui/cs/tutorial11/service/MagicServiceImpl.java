package id.ac.ui.cs.tutorial11.service;

import id.ac.ui.cs.tutorial11.model.MagicModel;
import id.ac.ui.cs.tutorial11.repository.MagicRepository;
import reactor.core.publisher.Flux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MagicServiceImpl implements MagicService {

    @Autowired
    private MagicRepository repo;

    public Flux<MagicModel> findAll(){
        return Flux.fromIterable(repo.findAll());
    }

    public MagicModel addMagic(MagicModel magicModel){
        return repo.save(magicModel);
    }

}