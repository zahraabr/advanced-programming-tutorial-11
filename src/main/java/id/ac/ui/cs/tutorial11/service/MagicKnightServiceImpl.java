package id.ac.ui.cs.tutorial11.service;

import id.ac.ui.cs.tutorial11.model.MagicKnightModel;
import id.ac.ui.cs.tutorial11.repository.MagicKnightRepository;
import reactor.core.publisher.Flux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MagicKnightServiceImpl implements MagicKnightService {

    @Autowired
    private MagicKnightRepository repo;

    @Override
    public Flux<MagicKnightModel> findAll() {
        return Flux.fromIterable(repo.findAll());
    }

    public MagicKnightModel addKnight(MagicKnightModel knight){
        return repo.save(knight);
    }
}